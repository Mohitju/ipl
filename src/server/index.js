const testFunction = require("./ipl.js");
const match = require("../data/matches.json");
const delivery = require("../data/deliveries.json");

const arr = testFunction.matchesPlayedPerYear(match);
const arr1 = testFunction.matchesWonPerTeamPerYear(match);
const arr2 = testFunction.extraRunsConcededPerTeamInYear2016(match,delivery);
const arr3 = testFunction.top10EconomicalBowlerInYears2015(match,delivery);

const fs=require('fs')

function saveJSON(filename = '',json = '""'){
    return fs.writeFileSync(filename,JSON.stringify(json))
}
saveJSON('../public/output/matchesPerYear.json',arr)

function saveJSON1(filename = '',json = '""'){
    return fs.writeFileSync(filename,JSON.stringify(json))
}
saveJSON1('../public/output/matchesWonPerTeamPerYear.json',arr1)

function saveJSON2(filename = '',json = '""'){
    return fs.writeFileSync(filename,JSON.stringify(json))
}
saveJSON2('../public/output/extraRunsConcededPerTeamInYear2016.json',arr2)

function saveJSON3(filename = '',json = '""'){
    return fs.writeFileSync(filename,JSON.stringify(json))
}
saveJSON3('../public/output/top10EconomicalBowlerInYears2015.json',arr3)

