function matchesPlayedPerYear(match){
    let arr = {};
    match.forEach(item =>{
        if(arr[item.season]===undefined)
        arr[item.season]=1;
        else
        arr[item.season]++;
    });
    return arr;
}
module.exports = {
    "matchesPlayedPerYear":matchesPlayedPerYear,
    "matchesWonPerTeamPerYear":matchesWonPerTeamPerYear,
    "extraRunsConcededPerTeamInYear2016":extraRunsConcededPerTeamInYear2016,
    "top10EconomicalBowlerInYears2015":top10EconomicalBowlerInYears2015
}

function matchesWonPerTeamPerYear(match){
    let arr = {};
    match.forEach(item =>{
        if(arr[item.season]===undefined){
        arr[item.season]={};
        arr[item.season][item.winner]=1;
        }
        else if(arr[item.season][item.winner]===undefined)
        arr[item.season][item.winner]=1;
        else
        arr[item.season][item.winner]++;
    });
    return arr;
}

function extraRunsConcededPerTeamInYear2016(match,delivery){
    var id = {};var extra = {};
    match.filter(item =>{
        if(item.season==='2016')
         id[item.id]=1;
    });
    delivery.forEach(item =>{
        if(id[item.match_id]){
            if(extra[item.bowling_team]===undefined)
            extra[item.bowling_team]= +item.extra_runs;
            else
            extra[item.bowling_team]+= +item.extra_runs;
        }
    });
    return extra;
}

function top10EconomicalBowlerInYears2015(match,delivery){
    var id = {};var arr = {};var over ={};
    match.filter(item =>{
        if(item.season==='2015')
         id[item.id]=1;
    });
    delivery.forEach(item =>{
        if(id[item.match_id]){
            if(arr[item.bowler]===undefined){
            arr[item.bowler]= +item.total_runs;
            over[item.bowler]= +1;
            }
            else{
            arr[item.bowler]+= +item.total_runs;
            over[item.bowler]+= +1;
            }
        }
    });
    for(let key in over){
        over[key]/=6;
    }
    for(let key in arr){
        arr[key]/=over[key];
    }
    var array = [];
    for(let key in arr){
        var object = {};
        object['bowler']=key;
        object['economy_rate']=arr[key];
        array.push(object);
    }
    array.sort(function(a,b){
        return a.economy_rate-b.economy_rate;
    });
    var economical ={};
    for(var i=0;i<10;i++){
        economical[array[i].bowler]=array[i].economy_rate;
    }
    return economical;
}